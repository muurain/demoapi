// app.js
// API server application


// Modules
var express         = require('express');
var bodyParser      = require('body-parser');
var methodOverride  = require('method-override');
var winston         = require('winston')

// App
var app = express();
require('./app/routes')(app)


// Configuration
var port        = process.env.PORT || 8080;
winston.level   = process.env.LOG_LEVEL || 'debug';

winston.log('info', 'Modules and configuration loaded succesfully.');


// Initialization
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(express.static(__dirname + '/public'));

app.listen(port);

winston.log('info', 'Server listening on port', port);


exports = module.exports = app;