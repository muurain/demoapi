// routes.js
// API routes

var os = require('os');

module.exports = function(app) {

    app.get('/header', function(req, res) {
        var info = req.headers;
        res.json(info);
    });

    app.get('/os', function(req, res) {
        var info = {
            "os" : os.type(),
            "platform": os.platform(),
            "arch": os.arch(),
            "uptime": os.uptime()
            };
        res.json(info);
    });

    app.get('/cpu', function(req, res) {
        var info = os.cpus();
        res.json(info);
    });

    app.get('/network', function(req, res) {
        var info = os.networkInterfaces();
        res.json(info);
    });

}